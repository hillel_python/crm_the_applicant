from bson.objectid import ObjectId
import email
import email.contentmanager
import email.utils
from flask import Flask, render_template, redirect, url_for, session, flash, request
from mongodb import Mongo

from celery_worker import send_email
from email_lib import EmailManager
from models import Vacancy, Event, EmailCreds, User
import database


app = Flask(__name__)
app.secret_key = 'Password123456789'


@app.route('/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        database.init_db()
        login_user = request.form.get('login')
        password_user = request.form.get('password')
        user_info = database.db_session.query(User).filter_by(login=login_user, password=password_user).first()
        if not user_info:
            flash('Unknown username or password')
            return redirect(url_for('login'))
        else:
            session['user_id'] = user_info.id
            session['login'] = user_info.login
            return redirect(url_for('show_user', user=user_info.name))
    return render_template('login.html')


@app.route('/logout/')
def logout():
    session.pop('login')
    return redirect(url_for('login'))


@app.route('/user/', methods=['GET'])
def show_user():
    current_user = session.get('login', None)
    return render_template('main_page.html', user=current_user)


@app.route('/registration/', methods=['POST', 'GET'])
def registration_user():
    database.init_db()
    if request.method == 'POST':
        name_user = request.form.get('name')
        login_user = request.form.get('login')
        email_user = request.form.get('email')
        password_user = request.form.get('password')
        current_user = User(name_user, login_user, password_user, email_user)
        database.db_session.add(current_user)
        database.db_session.commit()
        return redirect(url_for('login'))
    return render_template('registration.html')


@app.route('/vacancy/', methods=['GET', 'POST'])
def show_vacancy():
    database.init_db()
    mongo = Mongo()
    current_user = session.get('user_id', None)
    if request.method == 'POST':
        company = request.form.get('company')
        description = request.form.get('description')
        position_name = request.form.get('position_name')
        name_contact = request.form.get('name contact')
        phone_contact = request.form.get('phone contact')
        email_contact = request.form.get('email contact')
        status = 'New'
        comment = 'some comment'
        contacts_ids = mongo.create_new_contact(name_contact, phone_contact, email_contact)
        current_vacancy = Vacancy(status, company, contacts_ids,
                               description, position_name, comment, current_user)
        database.db_session.add(current_vacancy)
        database.db_session.commit()
        flash('Vacancy add successful')
    result = database.db_session.query(Vacancy).filter_by(user_id=current_user).all()
    return render_template('vacancy_add.html', vacancy_result=result)


@app.route('/vacancy/<vacancy_id>/', methods=['GET', 'POST'])
def vacancy(vacancy_id):
    database.init_db()
    mongo = Mongo()
    current_user = session.get('user_id', None)
    if request.method == 'POST':
        status = request.form.get('status')
        comment = request.form.get('comment')
        name_contact = request.form.get('name contact')
        phone_contact = request.form.get('phone contact')
        email_contact = request.form.get('email contact')
        contacts_ids = mongo.create_new_contact(name_contact, phone_contact, email_contact)
        database.db_session.query(Vacancy).filter_by(id=vacancy_id, user_id=current_user).\
            update({'status': status, 'comment': comment, "contacts_ids": contacts_ids})
        database.db_session.commit()
    qry = database.db_session.query(Vacancy).filter_by(id=vacancy_id, user_id=current_user).all()
    for contact in qry:
        contact_id = contact.contacts_ids
    contacts = mongo.connection().find_one({'_id': ObjectId(contact_id)})
    return render_template('vacancy_update.html', vacancy_select=qry,
                           vacancy_id=vacancy_id, contacts=contacts)


@app.route('/vacancy/<vacancy_id>/events/', methods=['GET', 'POST'])
def vacancy_id_events(vacancy_id):
    database.init_db()
    if request.method == 'POST':
        description = request.form.get('description')
        title = request.form.get('title')
        status = "New"
        due_to_date = '2023-02-10 21:11:35.537000'
        events_data = Event(vacancy_id, description, title, due_to_date, status)
        database.db_session.add(events_data)
        database.db_session.commit()
    result = database.db_session.query(Event).filter_by(vacancy_id=vacancy_id, ).all()
    return render_template('events_add.html', events_result=result, vacancy_id=vacancy_id)


@app.route('/vacancy/<vacancy_id>/events/<events_id>/', methods=['GET', 'POST'])
def vacancy_id_events_id(vacancy_id, events_id):
    database.init_db()
    if request.method == 'POST':
        status = request.form.get('status')
        due_to_date = request.form.get('deadline')
        database.db_session.query(Event).filter_by(id=events_id).\
            update({'status': status, 'due_to_date': due_to_date})
        database.db_session.commit()
    qry = database.db_session.query(Event).filter_by(id=events_id).all()
    return render_template('events_update.html', event_select=qry,
                           vacancy_id=vacancy_id, events_id=events_id)


@app.route('/user/mail/', methods=['GET', 'POST'])
def show_user_mail():
    current_user = session.get('user_id', None)
    user_settings = database.db_session.query(EmailCreds).filter_by(user_id=current_user).first()
    if not user_settings:
        return redirect(url_for('show_user_settings'))
    else:
        email_obj = EmailManager(
            user_settings.login,
            user_settings.email,
            user_settings.password,
            user_settings.smtp_server,
            user_settings.smtp_port,
            user_settings.pop_server1,
            user_settings.pop_port,
            user_settings.imap_server,
            user_settings.imap_port
        )
        if request.method == 'POST':
            send_to = request.form.get('send to')
            message = request.form.get('message')
            subject = request.form.get('subject')
            send_email.apply_async(args=[user_settings.id, send_to, subject, message])
            return 'The letter has been sent'
        current_email = email_obj.get_imap()
        email_message = email.message_from_string(str(current_email))
        return render_template('emails.html', current_email=email_message)


@app.route('/user/settings/', methods=['GET', 'POST'])
def show_user_settings():
    database.init_db()
    current_user = session.get('user_id', None)
    if request.method == "POST":
        email_user = request.form.get('email')
        login_user = request.form.get('login')
        password = request.form.get('password')
        pop_server = request.form.get('pop server')
        pop_port = request.form.get('pop port')
        smtp_server = request.form.get('smtp server')
        smtp_port = request.form.get('smtp port')
        imap_server = request.form.get('imap server')
        imap_port = request.form.get('imap port')
        current_settings = EmailCreds(current_user, email_user, login_user, password, pop_server,
                                      pop_port, smtp_server, smtp_port, imap_server, imap_port)
        database.db_session.add(current_settings)
        database.db_session.commit()
    result = database.db_session.query(EmailCreds).filter_by(user_id=current_user).all()
    return render_template('settings.html', result=result)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5031)
