import os

from celery import Celery

from email_lib import EmailManager
from models import EmailCreds
import database

RABBIT_HOST = os.environ.get('RABBIT_HOST', 'localhost')
celery = Celery('celery_worker', broker=f'pyamqp://guest@{RABBIT_HOST}//')


@celery.task(name="send_email")
def send_email(id_email_creds, recipient, subject, message):
    database.init_db()
    email_creds_details = database.db_session.query(EmailCreds).filter_by(id=id_email_creds).first()
    email_obj = EmailManager(
        email_creds_details.login,
        email_creds_details.email,
        email_creds_details.password,
        email_creds_details.smtp_server,
        email_creds_details.smtp_port,
        email_creds_details.pop_server1,
        email_creds_details.pop_port,
        email_creds_details.imap_server,
        email_creds_details.imap_port
    )
    email_obj.send_email(recipient, subject, message)


