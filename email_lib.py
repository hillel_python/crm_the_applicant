import certifi
import imaplib
import poplib
import smtplib
import ssl

from email.message import EmailMessage


class EmailManager:
    def __init__(self, login, email, password, smtp_server, smtp_port, pop_server, pop_port, imap_server, imap_port):
        self.login = login
        self.email = email
        self.password = password
        self.smtp_server = smtp_server
        self.smtp_port = smtp_port
        self.pop_server = pop_server
        self.pop_port = pop_port
        self.imap_server = imap_server
        self.imap_port = imap_port

    def send_email(self, recipient, subject, message):
        context = ssl.create_default_context(cafile=certifi.where())
        msg = EmailMessage()
        with smtplib.SMTP_SSL(self.smtp_server, self.smtp_port, context=context) as server:
            server.login(self.email, self.password)
            msg['From'] = self.email
            msg['To'] = recipient
            msg['Subject'] = subject
            msg.set_content(message)
            server.send_message(msg)

    def get_emails(self, message, protocol='imap'):
        if protocol == 'pop3':
            return self.get_pop(message)
        if protocol == 'imap':
            return self.get_imap()
        else:
            raise ValueError('unknown protocol')

    def get_imap(self):
        imap = imaplib.IMAP4_SSL(self.imap_server, self.imap_port)
        imap.login(self.email, self.password)
        imap.select('Inbox')
        typ, data = imap.search(None, 'ALL')
        ids = data[0]
        id_list = ids.split()
        latest_email_id = id_list[-1]
        result, data = imap.fetch(latest_email_id, "(RFC822)")
        raw_email = data[0][1]
        imap.close()
        imap.logout()
        return raw_email

    def get_pop(self, messages):
        pop = poplib.POP3_SSL(self.pop_server, self.pop_port)
        pop.user(self.email)
        pop.pass_(self.password)
        pop.stat()
        pop.list()
        result = []
        for message in messages:
            message_data = pop.retr(message)[1]
            result.append(str(message_data))
        pop.quit()
        return result







