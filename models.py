from datetime import datetime

from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Text

from database import Base


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    login = Column(String(50), nullable=False)
    password = Column(String(50), nullable=False)
    email = Column(String(120), nullable=True)

    def __init__(self, name, login, password, email):
        self.name = name
        self.login = login
        self.password = password
        self.email = email

    def __repr__(self):
        return f'<User {self.name} {self.login} {self.password} {self.email}>'


class EmailCreds(Base):
    __tablename__ = 'email_creds'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    email = Column(String(50), nullable=False)
    login = Column(String(50), nullable=False)
    password = Column(String(50), nullable=False)
    pop_server1 = Column(String(50), nullable=True)
    pop_port = Column(Integer, nullable=True)
    smtp_server = Column(String(50), nullable=False)
    smtp_port = Column(Integer, nullable=False)
    imap_server = Column(String(50), nullable=False)
    imap_port = Column(Integer, nullable=False)

    def __init__(self, user_id, email, login, password,
                 pop_server1, pop_port, smtp_server, smtp_port, imap_server, imap_port):
        self.user_id = user_id
        self.email = email
        self.login = login
        self.password = password
        self.pop_server1 = pop_server1
        self.pop_port = pop_port
        self.smtp_server = smtp_server
        self.smtp_port = smtp_port
        self.imap_server = imap_server
        self.imap_port = imap_port

    def __repr__(self):
        return f'{self.email}, {self.login}, {self.password}, {self.pop_server}, {self.pop_port},' \
               f' {self.smpt_server}, {self.smtp_port}, {self.imap_server}, {self.imap_port}  '


class Vacancy(Base):
    __tablename__ = 'vacancy'
    id = Column(Integer, primary_key=True, autoincrement=True)
    creation_date = Column(DateTime, default=datetime.utcnow)
    status = Column(String(50), nullable=False)
    company = Column(String(50), nullable=True)
    contacts_ids = Column(String(50), nullable=True)
    description = Column(Text, nullable=True)
    position_name = Column(String(100), nullable=True)
    comment = Column(String(100), nullable=False)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)

    def __init__(self, status, company,
                 contacts_ids, description, position_name, comment, user_id):
        self.status = status
        self.company = company
        self.contacts_ids = contacts_ids
        self.description = description
        self.position_name = position_name
        self.comment = comment
        self.user_id = user_id

    def __repr__(self):
        return f'{self.id}, Creation date - {self.creation_date:%d.%m.%Y}, Status -  {self.status}, ' \
               f'Company - {self.company}, Position name -  {self.position_name}, Description-  {self.description}, ' \
               f'Comment - {self.comment}, Contacts -  {self.contacts_ids}, User id - {self.user_id}'


class Event(Base):
    __tablename__ = 'event'
    id = Column(Integer, primary_key=True)
    vacancy_id = Column(Integer, ForeignKey('vacancy.id'))
    description = Column(Text, nullable=False)
    event_data = Column(DateTime, default=datetime.utcnow)
    title = Column(String(100), nullable=False)
    due_to_date = Column(String(50), nullable=True)
    status = Column(String(50), nullable=True)

    def __init__(self, vacancy_id, description, title, due_to_date, status):
        self.vacancy_id = vacancy_id
        self.description = description
        self.title = title
        self.due_to_date = due_to_date
        self.status = status

    def __repr__(self):
        return f' {self.id},  {self.vacancy_id}, {self.description}, {self.event_data:%d.%m.%Y},' \
               f'{self.title}, {self.due_to_date}, {self.status} '