import os

import pymongo


class Mongo:
    def __init__(self):
        self.localhost = os.environ.get('MONGO_HOST', 'localhost')
        self.host = self.localhost
        self.port = 27017
        self.username = 'root'
        self.password = 'example'
        self.database = 'crm_contacts'
        self.collection = 'contacts'

    def connection(self):
        client = pymongo.MongoClient(self.host, self.port, username=self.username, password=self.password)
        mongobase = client[self.database]
        contacts_collection = mongobase[self.collection]
        return contacts_collection

    def create_new_contact(self, name_contact, phone_contact, email_contact):
        contacts_ids = str(self.connection().insert_one(
            {"name": name_contact,
             "phone": phone_contact,
             "email": email_contact}
        ).inserted_id)
        return contacts_ids





